﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening23._2.Test
{
   public class MainWindowTest
    {
        [Test]
        public void Omtrek()
        {
            double straal = 3;

            double result = Math.Round((3 * 2 * Math.PI), 2);

            Assert.AreEqual((18.85), result);

        }
    }
}
