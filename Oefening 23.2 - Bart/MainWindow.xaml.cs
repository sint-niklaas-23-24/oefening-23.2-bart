﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_23._2___Bart
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnBereken_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cirkel mijnCirkel = new Cirkel(Convert.ToDouble(txtStraal.Text));
                lblOmtrek.Content = mijnCirkel.ToString();

                //lblOmtrek.Content = "Omtrek:" + Omtrek(Convert.ToDouble((txtStraal.Text).Replace(".", ","))).ToString().PadLeft(20);
                //lblOppervlakte.Content = "Oppervlakte:" + Oppervlakte(Convert.ToDouble((txtStraal.Text).Replace(".", ","))).ToString().PadLeft(15);
            }
            catch 
            {
                MessageBox.Show("De waarde van straal moet numeriek zijn.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        //private double Omtrek(double straal)
        //{
        //    return Math.Round((straal * 2 * Math.PI), 2);
        //}
        //private double Oppervlakte(double straal)
        //{
        //    return Math.Round((straal * straal * Math.PI), 2);
        //}
    }
}