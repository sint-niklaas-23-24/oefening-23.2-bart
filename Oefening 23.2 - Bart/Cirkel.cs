﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_23._2___Bart
{
    class Cirkel
    {
        private double _straal;

        public Cirkel() { }
        public Cirkel(double straal)
        {
            _straal = straal;
        }

        public double Straal
        {
            get { return _straal; }
            set { _straal = value; }
        }

        public double BerekenOmtrek()
        {
            return Straal * 2 * Math.PI;
        }
        public double BerekenOppervlakte()
        {
            return Straal * Straal * Math.PI;
        }
        public string FormattedOmtrek()
        {
            return Math.Round(BerekenOmtrek(), 2).ToString();
        }
        public string FormattedOppervlakte()
        {
            return Math.Round(BerekenOppervlakte(), 2).ToString();
        }
        public override string ToString()
        {
            return "Omtrek:" + FormattedOmtrek().PadLeft(20) + Environment.NewLine + "Oppervlakte:" + FormattedOppervlakte().PadLeft(15) + Environment.NewLine;
        }

    }
}
